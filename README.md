# latex-docker [![status-badge](https://ci.codeberg.org/api/badges/dminca/latex-docker/status.svg)](https://ci.codeberg.org/dminca/latex-docker)

Because it's impossible to find a maintained LaTeX image on the Internet. All
the images that you'll find are 2.609GB in size and guess how much malware
you can fit in that crap! You'll be surprised.

## Usage

Image is pushed at `dminca/latex` on [Docker Hub][2]

| Image | Size | Description |
| ----- | ---- | ----------- |
| `dminca/latex-slim:v1-${CI_COMMIT_SHA:0:8}` | `1.38GB` | slim image with only basic LaTeX packages and fonts |
| `dminca/latex-fat:v1-${CI_COMMIT_SHA:0:8}` | `3.22GB` | full LaTeX installation with full fonts support |

## Prereqs
Secret variables configured in Woodpecker CI
* `DOCKER_USERNAME`
* `DOCKER_PASSWORD`

For more information about the accepted pipeline arguments check out
https://drone-plugin-index.geekdocs.de/plugins/drone-docker-buildx/

[2]: https://hub.docker.com/r/dminca
